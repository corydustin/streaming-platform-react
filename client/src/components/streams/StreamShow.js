import React, { useEffect } from 'react';
import { connect } from 'react-redux';

import { fetchStream } from '../../actions';

const StreamShow = ({ stream, match, fetchStream }) => {

  useEffect(() => {
    fetchStream(match.params.id);
  }, [fetchStream, match])

  if(!stream) {
    return <div>Loading...</div>;
  }

  return (
    <div>
      <h1>{stream.title}</h1>
      <h5>{stream.description}</h5>
    </div>
  );

};

/**
 * Maps redux state to props of this react component
 */
const mapStateToProps = (state, ownProps) => {
  return {
    stream: state.streams[ownProps.match.params.id]
  };
};

/**
 * Hook up redux and all action creators needed to be utilized within this component
 */
export default connect(mapStateToProps, { fetchStream })(StreamShow);