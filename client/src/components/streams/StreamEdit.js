import React, { useEffect } from 'react';
import { connect } from 'react-redux';

import StreamForm from './StreamForm';
import { fetchStream, editStream } from '../../actions';

/**
 * React Function based Component for rendering the edit form for a specific stream
 */
const StreamEdit = ({ stream, match, fetchStream, editStream }) => {

  useEffect(() => {

    fetchStream(match.params.id);

  }, [fetchStream, match])

  const onSubmit = (formValues) => {
    console.log(formValues);
    editStream(match.params.id, formValues);
  };

  if(!stream) {
    return <div>Loading...</div>
  }

  return (
    <div>
      <h3>Edit a Stream</h3>
      <StreamForm 
        initialValues={{ title: stream.title, description: stream.description }} // Prop that is read from Redux Form to add initial values to form for fields provided
        onSubmit={onSubmit}
      />
    </div>
  );

};

/**
 * Maps redux state to props of this react component
 */
const mapStateToProps = (state, ownProps) => {
  return {
    stream: state.streams[ownProps.match.params.id] // Retrieve the id query param from the url path and retrieve stream of id
  };
};

/**
 * Hook up redux and all action creators needed to be utilized within this component
 */
export default connect(mapStateToProps, { fetchStream, editStream })(StreamEdit);