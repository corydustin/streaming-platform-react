import React from 'react';
import { Form, Field } from 'react-final-form';

/**
 * React Function Component responsible for rendering a form for editing a stream. 
 * Utilizes React Final Form
 */
const StreamForm = (props) => {

  const renderError = ({ error, touched }) => {
    // Only show validation error message if the user has touched the input field
    if(touched && error) {
      return (
        <div className="ui error message">
          <div className="header">
            {error}
          </div>
        </div>
      );
    }
  }

  // Renders an input field for the redux form displayed within this Component
  const renderFormInput = ({ input, label, meta }) => { // Destruct out form props that were passed via redux-form
    const inputClassName = `field ${ meta.error && meta.touched ? 'error': '' }`

    return (
      <div className={inputClassName}>
      <label>{label}</label>
        <input value={input.value} onChange={input.onChange} autoComplete="off" /> 
        {renderError(meta)}
      </div>
    );
  }

  // Handle Submission of form
  const onSubmit = (formValues) => {
    props.onSubmit(formValues);
  }

  // Validate input that is submitted on the form
  const validate = (formValues) => {

    const errors = {};
  
    if(!formValues.title) {
      errors.title = 'Title is required';
    }
  
    if(!formValues.description) {
      errors.description = 'Description is required';
    }
  
    return errors;
  }

  const renderForm = (props) => {
    return ( // handleSubmit comes from redux-form to handle the submission of the form
      <form 
        onSubmit={props.handleSubmit} 
        className="ui form error"
      > 
        <Field 
          name="title" 
          component={renderFormInput} 
          label="Enter Title" 
        />
        <Field 
          name="description" 
          component={renderFormInput} 
          label="Enter Description" 
        />
        
        <button className="ui button primary">Submit</button>
      </form>
    );
  }

  return (
    <Form
      initialValues={props.initialValues}
      onSubmit={onSubmit}
      validate={validate}  
      render={renderForm}
    >
    </Form>
  );
  

};
export default StreamForm;