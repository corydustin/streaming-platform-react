import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { fetchStreams } from '../../actions';

/**
 * React Function based component for rendering a list of streams retrieved
 */
const StreamList = ({ fetchStreams, streams, currentUserId, isSignedIn }) => {

  // On mount of this Component, attempt to fetch all current streams within the DB
  useEffect(() => {

    fetchStreams();

  }, [fetchStreams]);

  // Renders the Edit/Delete buttons if the stream belongs to the currently logged in user
  const renderAdmin = (stream) => {
    // If the current logged in user isnt the one who created the stream
    // dont show the edit/delete buttons
    if(stream.userId !== currentUserId) {
      return; 
    }

    // Route the user to the specific edit/delete pages and put the stream id at the end of the url path
    return (
      <div className="right floated content">
        <Link to={`/streams/edit/${stream.id}`} className="ui button primary">Edit</Link>
        <Link to={`/streams/delete/${stream.id}`} className="ui button negative">Delete</Link>
      </div>
    );
  };

  // Renders the list of streams retrieved from the fetchStreams action creator
  const renderList = () => {
    return streams.map(stream => {
      return (
        <div className="item" key={stream.id}>
          {renderAdmin(stream)}
          <i className="large middle aligned icon camera" />
          <div className="content">
            <Link to={`/streams/${stream.id}`} className="header">
              {stream.title}
            </Link>
            <div className="description">
              {stream.description}
            </div>
          </div>
        </div>
      );
    });
  };

  // Renders the create button for creating a new stream
  const renderCreate = () => {
    if(isSignedIn) {
      return (
        <div style={{ textAlign: 'right' }}>
          <Link to="/streams/new" className="ui button primary">
            Create Stream
          </Link>
        </div>
      );
    }
  };

  return (
    <div>
      <h2>Streams</h2>
      <div className="ui celled list">
        {renderList()}
      </div>
      {renderCreate()}
    </div>
  );

};

/**
 * Maps redux state to props of this react component
 */
const mapStateToProps = (state) => {
  return {
    streams: Object.values(state.streams),
    currentUserId: state.auth.userId,
    isSignedIn: state.auth.isSignedIn
  }
}

/**
 * Hook up redux and all action creators needed to be utilized within this component
 */
export default connect(mapStateToProps, { fetchStreams })(StreamList);