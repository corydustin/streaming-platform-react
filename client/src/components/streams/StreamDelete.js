import React, { useEffect} from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import Modal from '../Modal';
import { fetchStream, deleteStream } from '../../actions';
import history from '../../history';

/**
 * Function based React Component responsible for rendering the Delete Stream page
 */
const StreamDelete = ({stream, match, fetchStream, deleteStream}) => {

  // Fetch the stream we are trying to delete on load of this component
  useEffect(() => {
    fetchStream(match.params.id)
  }, [fetchStream, match]);

  const actions = (
    <>
      <button 
        onClick={ () => deleteStream(match.params.id) } 
        className="ui button negative">Delete
      </button>
      <Link to="/" className="ui button">Cancel</Link>
    </>
  );

  const renderContent = () => {
    if (!stream) {
      return 'Are you sure you want to delete this Stream?';
    }

    return (
      <div>
        Are you sure you want to delete the Stream with title: <b>{stream.title}</b> ?
      </div>
    );
  }

  // JSX to render this Component
  return (
    <Modal 
      title="Delete Stream"
      content={renderContent()}
      actions={actions}
      onDismiss={() => history.push('/')}
    />
  );

};

/**
 * Maps redux state to props of this react component
 */
const mapStateToProps = (state, ownProps) => {
  return {
    stream: state.streams[ownProps.match.params.id] // Retrieve the id query param from the url path and retrieve stream of id
  };
};

/**
 * Hook up redux and all action creators needed to be utilized within this component
 */
export default connect(mapStateToProps, { fetchStream, deleteStream })(StreamDelete);