import React from 'react';
import { connect } from 'react-redux';
import { createStream } from '../../actions';
import StreamForm from './StreamForm';

/**
 * React Component that will handle rendering the Create Stream UI, 
 * enabling users to create new streams. Example utilizing Class based components
 * with React
 */
class StreamCreate extends React.Component {

  onSubmit = (formValues) => {
    this.props.createStream(formValues);
  }

  render() {
    return ( 
      <div>
        <h3>Create a Stream</h3>
        <StreamForm onSubmit={this.onSubmit} initialValues={{}}/>
      </div>
    );
  }

};

export default connect(null, { createStream })(StreamCreate);