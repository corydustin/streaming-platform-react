import React from 'react';
import { Router, Route, Switch } from 'react-router-dom';

// Component Imports
import Header from './Header';
import StreamCreate from './streams/StreamCreate';
import StreamDelete from './streams/StreamDelete';
import StreamEdit from './streams/StreamEdit';
import StreamList from './streams/StreamList';
import StreamShow from './streams/StreamShow';
import history from '../history';

// Styling
import '../style/App.css'

/**
 * Main React App Component to act as entry point of app. Utilzes React Router Dom 
 * to determine what componenet should be rendered based on the route entered
 */
const App = () => {
  return (
    <div className="ui container">
      <Router history={history}>
        <Header /> 
        <Switch>
          <Route path="/" exact component={StreamList}/>
          <Route path="/streams/new" exact component={StreamCreate}/>
          <Route path="/streams/edit/:id" exact component={StreamEdit}/>
          <Route path="/streams/delete/:id" exact component={StreamDelete}/>
          <Route path="/streams/:id" exact component={StreamShow}/>
        </Switch>
      </Router> 
    </div>
  );
};

export default App;