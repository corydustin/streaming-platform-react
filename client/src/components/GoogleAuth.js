import React from 'react';
import { connect } from 'react-redux';

import { signIn, signOut } from '../actions';

/**
 * Google Auth React Component that will attempt to retrieve a users email and log 
 * them in within the Streaming platform
 */
class GoogleAuth extends React.Component {
  
  componentDidMount() {
    this._loadGapiLibrary();
  }

  // Loads the Google API library for OAuth2 connection
  _loadGapiLibrary() {
    // Load the google api for OAuth2
    window.gapi.load('client:auth2', () => {
      // Once Google API has been loaded, initialize with our client ID
      window.gapi.client.init({
        clientId: '908205873130-67epd5v77obna58740kfe5m6j6lmsqs4.apps.googleusercontent.com',
        scope: 'email',
        plugin_name: 'Viral Streaming'
      })
      .then(() => {
        this.auth = window.gapi.auth2.getAuthInstance();

        // Set state on if the user is signed in or not
        this.onAuthChange(this.auth.isSignedIn.get());
        
        // Listen for when the users auth status changes 
        this.auth.isSignedIn.listen(this.onAuthChange); 
      });
    });
  }

  // Callback for when authentication changes
  onAuthChange = (isSignedIn) => {
    if(isSignedIn) {
      this.props.signIn(this.auth.currentUser.get().getId());
    } else {
      this.props.signOut();
    }
  };

  // Signs out the user via Google OAuth
  onSignInClick = () => {
    this.auth.signIn();
  };

  // Signs out the user via Google OAuth
  onSignOutClick = () => {
    this.auth.signOut();
  }

  // Returns JSX representing the Google OAuth button
  googleAuthButton(content, onButtonClick) {
    return (
      <button className="ui red google button" onClick={onButtonClick}>
        <i className="google icon" />
        {content}
      </button>
    );
  }

  // Determines how the Google OAuth button should be rendered
  renderAuthButton() {
    if(this.props.isSignedIn === null) {
      return null;
    } else if(this.props.isSignedIn) {
      return this.googleAuthButton('Sign Out', this.onSignOutClick);
    } else {
      return this.googleAuthButton('Sign In with Google', this.onSignInClick);
    }
  }

  // Main render method for this component
  render() {
    return (
      <div>{this.renderAuthButton()}</div>
    );
  }

}

/**
 * Maps redux state to props of this react component
 */
const mapStateToProps = (state) => {
  return { 
    isSignedIn: state.auth.isSignedIn 
  };
}

/**
 * Hook up redux and all action creators needed to be utilized within this component
 */
export default connect(mapStateToProps, { signIn, signOut })(GoogleAuth);