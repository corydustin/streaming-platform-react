import React from 'react';
import { Link } from 'react-router-dom';

import GoogleAuth from './GoogleAuth';

/**
 * Header component to render at top of page
 */
const Header = () => {

  return (
    <div className="ui secondary pointing menu">
      <Link to="/" className="item">
        ViraL Streaming
      </Link>
      <div className='right menu'>
        <Link to="/" className="item">
          All Streams
        </Link>
        <GoogleAuth />
      </div>
    </div>
  );

};

export default Header;