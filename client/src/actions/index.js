import streams from '../apis/stream';
import history from '../history';

/**
 * Action Types to utilize within Action Creators
 */
import { 
  SIGN_IN, 
  SIGN_OUT, 
  CREATE_STREAM,
  FETCH_STREAM,
  FETCH_STREAMS, 
  DELETE_STREAM,
  EDIT_STREAM 
} 
from './types';

/**
 * Sign in a user within the streaming platform
 * @param {string} userId 
 * @returns Action containing signin data
 */
export const signIn = (userId) => {
  return {
    type: SIGN_IN,
    payload: {
      userId
    }
  };
};

/**
 * Sign out a user within the streaming platform
 * @returns Action containing expected sign out data 
 */
export const signOut = () => {
  return {
    type: SIGN_OUT
  };
};

/**
 * Retrieves a list of streams for a logged in user
 */
export const fetchStreams = () => async (dispatch) => {

  const response = await streams.get('/streams');

  dispatch({
    type: FETCH_STREAMS,
    payload: response.data
  });
};

/**
 * Retrieves a specific stream based on the id provided
 */
export const fetchStream = (id) => async (dispatch) => {

  const response = await streams.get(`/streams/${id}`);

  dispatch({
    type: FETCH_STREAM,
    payload: response.data
  });

};

/**
 * Creates a new stream based on the formValues provided 
 * @param {FormValues} formValues the values to utilize to create a new stream
 */
export const createStream = (formValues) => async (dispatch, getState) => {
  const { userId } = getState().auth;
  const response = await streams.post('/streams', {...formValues, userId});

  dispatch({
    type: CREATE_STREAM,
    payload: response.data
  });

  // Navigate the user back to the root path when a sucessful stream is created
  history.push("/");
};

/**
 * Edits a specific stream with the form values provided
 * @param {string} id 
 * @param {FormValues} formValues 
 * @returns 
 */
export const editStream = (id, formValues) => async (dispatch) => {

  const response = await streams.patch(`/streams/${id}`, formValues);

  dispatch({
    type: EDIT_STREAM,
    payload: response.data
  });

  // Navigate the user back to the root path when a sucessful stream is created
  history.push("/");
};

/**
 * Deletes a specified stream
 * @param {string} id 
 */
export const deleteStream = (id) => async (dispatch) => {
  await streams.delete(`/streams/${id}`);

  dispatch({
    type: DELETE_STREAM,
    payload: id
  });

  // Navigate the user back to the root path when a sucessful stream is created
  history.push("/");
};