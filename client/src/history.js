import { createBrowserHistory } from 'history'

// Creating a history instance so we can set navigation within action creators
export default createBrowserHistory();