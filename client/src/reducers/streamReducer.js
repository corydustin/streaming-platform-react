import _ from 'lodash';

import { 
  FETCH_STREAM, 
  FETCH_STREAMS, 
  CREATE_STREAM, 
  EDIT_STREAM,
  DELETE_STREAM
} from "../actions/types";

const streamReducer = (state={}, action) => {

  switch(action.type) {
    case FETCH_STREAMS:
      return { ...state, ..._.mapKeys(action.payload, 'id')}; // Convert incoming payload array to object with element's key as the id attribute
    case FETCH_STREAM:
      return { ...state, [action.payload.id]: action.payload };
    case CREATE_STREAM: 
      return { ...state, [action.payload.id]: action.payload };
    case EDIT_STREAM:
      return { ...state, [action.payload.id]: action.payload };
    case DELETE_STREAM:
      return _.omit(state, action.payload); // Return everything in current state excluding the stream that was just deleted
    default: 
      return state;
  }

};

export default streamReducer;