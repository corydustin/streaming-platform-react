import axios from 'axios';

/**
 * Custom Axios instance to interact with local backend API that will 
 * store all data related to streams created.
 */
export default axios.create({
  baseURL: "http://localhost:3001"
});